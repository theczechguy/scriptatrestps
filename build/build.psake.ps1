using namespace System.Management.Automation.Language
#Requires -Module "PSake"
#Requires -Module "BuildHelpers"

$InformationPreference = 'continue'
Write-Information "Script root : $PSScriptRoot"

Task Build -depends Clean,CreateReleaseFolder, CreateModuleManifest, CreateModule {
}
Task Clean {
    Write-Information 'Removing release folder'
    if (test-path -Path $PSScriptRoot\release) {
        Remove-Item -Path $PSScriptRoot\release -Recurse -Force
    }
}
Task CreateReleaseFolder {
    write-information 'Creating release folder'
    If (!(Test-Path -Path $PSScriptRoot\release)) {
        $null = New-Item -ItemType Directory -Path $PSScriptRoot\release
    }
}

Task CreateModule {
    write-information 'Creating base .psm1 file'
    $moduleFile = New-Item -Path $PSScriptRoot\release\psar.psm1 -ItemType File -Force
    $moduleTemplate = Get-Content -Path $PSScriptRoot\template.psm1 -Raw
    Set-Content -Path $moduleFile.FullName -Value $moduleTemplate -Encoding utf8

    Write-Information 'Fetching public functions'
    $functions = Get-ChildItem -Path ..\src\public -File -Recurse
    foreach ($function in $functions) {
        Write-Information "`t [+] $($function.BaseName)"
        Get-Content -Path $function.FullName -Raw | Add-Content -Path $moduleFile.FullName -Encoding utf8
    }
}

Task CreateModuleManifest {
    $exportFunctions = @()

    Write-Information "Creating new module manifest"
    $null = Copy-Item -Path $PSScriptRoot\PSar.psd1 -Destination $PSScriptRoot\release\PSar.psd1 -force
    $publicFunctions = Get-ChildItem -Path ..\src\public -File -Recurse -Filter "*.ps1"
    foreach ($functionFile in $publicFunctions) {
        $ast = [System.Management.Automation.Language.Parser]::ParseFile($functionFile.FullName, [ref]$null, [ref]$null)
        $functions = $ast.FindAll({
            $args[0] -is [System.Management.Automation.Language.FunctionDefinitionAst]
        }, $true)
        if ($functions.name) {
            $exportFunctions += $functions.name
        }
    }
    Set-ModuleFunction -Name $PSScriptRoot\release\PSar.psd1 `
        -FunctionsToExport ($exportFunctions | sort)
}

Task default -depends Build