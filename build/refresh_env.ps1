Write-Output ('=' * 50)
Write-Output 'remove module'
remove-module psar -force -ErrorAction SilentlyContinue

Write-Output 'run psake'
invoke-psake .\build.psake.ps1

Write-Output 'import module'
import-module .\release\psar.psm1 -force
Write-Output ('=' * 50)