# --- Clean up variables on module removal
$ExecutionContext.SessionState.Module.OnRemove = {
    if ($script:SarConnection) {
        Remove-Variable -Name SarConnection -Force -ErrorAction SilentlyContinue   
    }
}