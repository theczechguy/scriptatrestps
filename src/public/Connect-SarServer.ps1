function Connect-SarServer {
    [CmdletBinding(DefaultParameterSetName = 'Username')]
    param (
        [Parameter(Mandatory)]
        [string] $ServerUrl,

        [Parameter(Mandatory, ParameterSetName = 'Username')]
        [string] $Username,

        [Parameter(Mandatory, ParameterSetName = 'Username')]
        [securestring] $Password,

        [Parameter(Mandatory, ParameterSetName = 'Credentials')]
        [pscredential] $Credentials,

        [Parameter(Mandatory, ParameterSetName = 'Token')]
        [string] $AccessToken
    )
    begin{
        $loginUri = '{0}/api/authentication/login' -f $ServerUrl
        Write-Verbose "[$($MyInvocation.MyCommand.Name) login uri : $loginUri"
        $requestHeaders = @{
            Accept = "application/json"
            'Content-Type' = "application/json"
        }
        $requestBody = @{}
    }
    process {
        if ($PSBoundParameters.ContainsKey('Credentials')) {
            $requestBody.Add('Username', $Credentials.UserName)
            $requestBody.Add('Password', $Credentials.GetNetworkCredential().Password)
        }
        if ($PSBoundParameters.ContainsKey('Username')){
            $requestBody.Add('Username', $Credentials.UserName)
            $requestBody.Add('Password', (ConvertFrom-SecureString -SecureString $Password))
        }

        $token
        $tokenExpiration
        $refreshToken
        $refreshTokenExpiration

        if ($PSBoundParameters.ContainsKey('AccessToken')) {
            $token = $AccessToken
        } else {
            $loginResponse = Invoke-RestMethod -Uri $loginUri `
                -Method Post `
                -Body ($requestBody | ConvertTo-Json) `
                -Headers $requestHeaders
            $token = $loginResponse.token
            $tokenExpiration = [datetime]$loginResponse.tokenExpiration
            $refreshToken = $loginResponse.refreshToken
            $refreshTokenExpiration = [datetime]$loginResponse.refreshTokenExpiration
        }

        $Script:SarConnection = [PSCustomObject] @{
            Server = $ServerUrl
            AccessToken = $token
            RefreshToken = $refreshToken
            AccessTokenExpiration = $tokenExpiration
            RefreshTokenExpiration = $refreshTokenExpiration
        }
        if (!$PSBoundParameters.ContainsKey('AccessToken')) {
            Get-SarUser -Username $requestBody.username
        } else {
            Write-Information 'Access token saved'
        }
    }
}