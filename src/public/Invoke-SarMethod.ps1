function Invoke-SarMethod {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [ValidateSet('GET','POST','PUT','DELETE')]
        [string] $Method,

        [Parameter(Mandatory)]
        [string] $Uri,

        [Parameter(Mandatory = $false)]
        [ValidateNotNullOrEmpty()]
        [string] $Body
    )
    process {
        if (-not $Script:SarConnection){
            throw "Connection has not been established yet. Please run Connect-SarServer first to create it"
        }
        $fullUri = '{0}{1}' -f $Script:SarConnection.Server, $Uri

        $requestHeaders = @{
            Accept = "application/json"
            'Content-Type' = "application/json"
            Authorization = 'Bearer {0}' -f $Script:SarConnection.AccessToken
        }

        $requestParams = @{
            Method = $Method
            Uri = $fullUri
            Headers = $requestHeaders
        }

        if ($PSBoundParameters.ContainsKey('Body')) {
            $requestParams.Add('Body', $Body)
        }

        try {
            Invoke-RestMethod @requestParams
        }
        catch {
            $responseHeader = $_.exception.Response.Headers.WwwAuthenticate.parameter
            if ($responseHeader) {
                Write-Warning $responseHeader
            }
            throw $_
        }
        finally {
            # there seems to be bug in invoke-restmethod where it doesnt complete the tcp session close after certain calls
            # needs to be rechecked again
            $ServicePoint = [System.Net.ServicePointManager]::FindServicePoint($fullUri)
            $ServicePoint.CloseConnectionGroup("") | Out-Null
        }
    }
}