function Get-SarScriptJob {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory,
            ValueFromPipeline,
            ValueFromPipelineByPropertyName,
            ParameterSetName = 'id')]
        [Alias('jobId')]
        [string]
        $Id,

        [Parameter(Mandatory, ParameterSetName = 'all')]
        [switch] $ListAll
    )
    process {
        if ($ListAll.IsPresent) {
            $apiUrl = 'api/scriptjob'
            Invoke-SarMethod -Method GET -Uri $apiUrl
        } else {
            foreach ($jobid in $Id) {
                $apiUrl = '/api/scriptjob/{0}' -f $jobid
                Invoke-SarMethod -Method GET -Uri $apiUrl
            }
        }
    }
}