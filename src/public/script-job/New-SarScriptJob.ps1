function New-SarScriptJob {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory,
            ValueFromPipeline,
            ValueFromPipelineByPropertyName)]
        [Alias('id')]
        [string]
        $ScriptId,
        [Parameter(Mandatory = $false)]
        [string]
        $ScriptParameters
    )    
    process {
        foreach ($id in $ScriptId) {
            $apiUrl = '/api/scriptjob/{0}' -f $id
            $invokeArgs = @{
                Method = 'POST'
                Uri = $apiUrl
            }
            if ($PSBoundParameters.ContainsKey('ScriptParameters')) {
                $invokeArgs.Add('ScriptParameters', $ScriptParameters)
            }
            Invoke-SarMethod @invokeArgs
        }
    }
}