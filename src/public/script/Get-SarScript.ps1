function Get-SarScript {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory, ParameterSetName = 'name')]
        [string]
        $ScriptName,

        [Parameter(Mandatory, ParameterSetName = 'id')]
        [string]
        $ScriptId,

        [Parameter(Mandatory, ParameterSetName = 'all')]
        [switch]
        $ListAll
    )
    process {
        if ($ListAll.IsPresent) {
            $apiUrl = '/api/script'
            Invoke-SarMethod -Method GET -Uri $apiUrl
        } else {
            $invokeArgs = @{
                Method = 'GET'
            }
            if ($PSBoundParameters.ContainsKey('ScriptId')) {
                $apiUrl = '/api/script/{0}' -f $ScriptId
                $invokeArgs.Add('Uri', $apiUrl)
            }
            if ($PSBoundParameters.ContainsKey('ScriptName')) {
                $apiUrl = '/api/script/byname'
                $requestBody = @{
                    name = $ScriptName
                } | ConvertTo-Json
                $invokeArgs.Add('Body',$requestBody)
                $invokeArgs.Add('Uri', $apiUrl)
            }
            Invoke-SarMethod @invokeArgs
        }
    }
}