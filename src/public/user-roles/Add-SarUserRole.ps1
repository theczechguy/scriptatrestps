function Add-SarUserRole {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory,
            ValueFromPipeline,
            ValueFromPipelineByPropertyName,
            ParameterSetName = 'username')]
        [ValidateNotNullOrEmpty()]
        [string] $Username,

        [Parameter(Mandatory)]
        [string]
        $Rolename
    )    
    begin {
        $requestBody = @{
            rolename = $Rolename
        } | ConvertTo-Json
    }
    process {
        foreach ($user in $Username) {
            Write-Information "Adding role: $Rolename to user: $user"
            $apiUrl = '/api/users/{0}/addrole' -f $user
            Invoke-SarMethod -Method POST -Uri $apiUrl -Body $requestBody
        }
    }
}