function Get-SarUserRoles {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory,ValueFromPipeline,ValueFromPipelineByPropertyName = $true)]
        [string[]] $username
    )
    process {
        foreach ($user in $Username) {
            if ($user.username) {
                $usertoFind = $user.usernmae
            } else {
                $usertoFind = $user
            }
            Write-Verbose "user to find:"
            Write-Verbose $usertoFind
            $apiUrl = '/api/users/{0}/roles' -f $usertoFind
            Invoke-SarMethod -Method GET -Uri $apiUrl
        }   
    }
}