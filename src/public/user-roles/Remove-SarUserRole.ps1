function Remove-SarUserRole {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory,
            ValueFromPipeline,
            ValueFromPipelineByPropertyName,
            ParameterSetName = 'username')]
        [ValidateNotNullOrEmpty()]
        [string] $Username,

        [Parameter(Mandatory)]
        [string]
        $Rolename
    )    
    begin {
        $requestBody = @{
            rolename = $Rolename
        } | ConvertTo-Json
    }
    process {
        foreach ($user in $Username) {
            Write-Information "Removing role: $Rolename from user: $user"
            $apiUrl = '/api/users/{0}/removerole' -f $user
            Invoke-SarMethod -Method POST -Uri $apiUrl -Body $requestBody
        }
    }
}