function Get-SarUser {
    [CmdletBinding(DefaultParameterSetName = 'username')]
    param (
        [Parameter(Mandatory,
            ValueFromPipeline,
            ValueFromPipelineByPropertyName,
            ParameterSetName = 'username')]
        [ValidateNotNullOrEmpty()]
        [string] $Username,

        [Parameter(Mandatory, ParameterSetName = 'all')]
        [switch] $ListAll
    )    
    process {
        if ($ListAll.IsPresent) {
            Invoke-SarMethod -Method GET -Uri '/api/users'
        } else {
            foreach ($user in $Username) {
                $apiUrl = '/api/users/{0}' -f $user
                Invoke-SarMethod -Method GET -Uri $apiUrl
            }   
        }
    }
}