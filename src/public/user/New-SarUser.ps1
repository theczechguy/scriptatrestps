function New-SarUser {
    [CmdletBinding()]
    param (

        [Parameter(Mandatory,  ValueFromPipeline)]
        [ValidateNotNullOrEmpty()]
        [pscredential] $Credential
    )    
    begin {
        $apiUrl = '/api/users/register'
    }
    process {
        foreach ($cred in $Credential) {
            $requestBody = @{
                username = $cred.UserName
                password = $cred.GetNetworkCredential().Password
            }
            Invoke-SarMethod -Method POST -Uri $apiUrl -Body ($requestBody | ConvertTo-Json)
        }
    }
}