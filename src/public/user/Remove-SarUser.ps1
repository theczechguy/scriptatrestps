function Remove-SarUser {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory, ValueFromPipeline, ValueFromPipelineByPropertyName)]
        [string]
        $Username
    )    
    process {
        foreach ($user in $Username) {
            $apiUrl = '/api/users/{0}' -f $user
            Invoke-SarMethod -Method DELETE -Uri $apiUrl
        }
    }
}